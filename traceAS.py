import argparse
import socket
import json
from urllib.request import urlopen

ECHO_QUERY = b'\x08\x00\xf7\xd7\x00\x01\x00\x27'

PRIVATE_NETS = {
    ('10.0.0.0', '10.255.255.255'),
    ('172.16.0.0', '172.31.255.255'),
    ('192.168.0.0', '192.168.255.255'),
    ('127.0.0.0', '127.255.255.255')}


def trace_as(destination_ip, hops):
    current_ip = None
    ttl = 1
    timeout = 30
    connection = socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP)
    connection.settimeout(timeout)
    while ttl != hops and current_ip != destination_ip:
        connection.setsockopt(socket.SOL_IP, socket.IP_TTL, ttl)
        connection.sendto(ECHO_QUERY, (destination_ip, 1))
        try:
            packet, address = connection.recvfrom(1024)
            current_ip = address[0]
            info = f"{ttl}. {current_ip} : "
            if is_public(current_ip):
                info += get_info(current_ip)
            print(info)
        except socket.timeout:
            print(f"{ttl}. *** Time is out.")
        ttl += 1
    connection.close()


def is_public(ip):
    for addr in PRIVATE_NETS:
        if addr[0] <= ip <= addr[1]:
            return False
    return True


def get_info(ip):
    data = json.loads(urlopen(f'http://ipinfo.io/{ip}/json').read())
    keys = ["country", "region", "city", "org"]
    for key in keys:
        if not data.get(key):
            keys.remove(key)
    return str.join(', ', list(map(data.get, keys)))


def main():
    parser = argparse.ArgumentParser(description='Simple AS tracer')
    parser.add_argument('destination', type=str, help='Destination ip')
    parser.add_argument('--hops', default=30, type=int, help='Number of hops')
    args = parser.parse_args()
    name = ''
    try:
        name = socket.gethostbyname(args.destination)
    except:
        print("Check your internet connection or validity of the internet address")
    else:
        trace_as(name, args.hops)



if __name__ == '__main__':
    main()
